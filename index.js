// console.log("Hello World");

// JavaScript Synchronous vs Asynchronous
	// JavaScript - Synchronous - can execute one statement/line of code at a time.

	console.log("Hello World!");
	// conosle.log("Hello Again!");
	console.log("Goodbye.");

	console.log("Hello World!");
	/*for (let i = 0; i <= 1500; i++){
		console.log(i);
	}*/
	console.log("Hello Again!");

// Getting all post
	//fetch() - fetch request; the fetch API allows programmers to asynchronously request for a resource(data).
	/*syntax: 
		console.log(fetch(https://jsonplaceholder.typicode.com/posts));
	*/
	fetch("https://jsonplaceholder.typicode.com/posts")
	// we use "json" method from the response object to convert data into JSON format.
	.then( (response) => response.json())
	.then( (json) => console.log(json));

	// "async" and "await" keywords in another approach that can be used to achieve asynchronous"
	// used in functions to indicate which portions of the code should be awaited for.
	async function fetchData(){
		let result = await fetch("https://jsonplaceholder.typicode.com/posts");
		console.log(result);
		console.log(typeof result);
		console.log(result.body);

		let json = await result.json();
		console.log(json);
	}
	fetchData();

// Getting a specific post
	fetch("https://jsonplaceholder.typicode.com/posts/1")
	.then((response) => response.json())
	.then((json) => console.log(json));

// Creating a post
/*
syntax:
	fetch("URL", options)
	.then((response) = > {})
	.then((response) = > {})
*/
	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			title: "New Post",
			body: "Hello World",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));


// updating a post using put method
	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: "PUT",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			id: 1,
			title: "Updated Post",
			body: "Hello Again",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

// patch method - update a specific post / specific property in a post
	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: "PATCH",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			title: "Corrected Post",
			
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

	// deleting a post

	// fetch("https://jsonplaceholder.typicode.com/posts/1" {
	// 	method: "DELETE"
	// });

// filtering post
/*
syntax(individual parameter):
	'url?parameterName=value'

syntax(multiple parameter):
	url?paramA=valueA&paramB=valueB

*/
	fetch("https://jsonplaceholder.typicode.com/posts?userId=2")
	.then((response) => response.json())
	.then((json) => console.log(json));

// retrieving comments on specific post
	fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
	.then((response) => response.json())
	.then((json) => console.log(json));

